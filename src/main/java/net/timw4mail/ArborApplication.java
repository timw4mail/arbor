package net.timw4mail;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;

/**
 * Main class of Arbor
 */
@Configuration
public class ArborApplication {
	public static void Main(String[] args) {
		ApplicationContext context =
			new AnnotationConfigApplicationContext(ArborApplication.class);
	}
}